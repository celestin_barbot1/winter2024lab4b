public class Panda{
	private int age;
	private double weight;
	private String name;

	public Panda(int age, double weight, String name){
			this.age =  age;
			this.weight =  weight;
			this.name =  name;
	}

	public void setname(String name){this.name = name;}
	
	public int getage(){return this.age;}
	public double getweight(){return this.weight;}
	public String getname(){return this.name;}


	public void isFat(){
		if (this.weight >= 100.0){
			System.out.println(this.name + " is " + this.weight + " kg heavy. He is a fat panda.");
		}
		else{
			System.out.println(this.name + " is " + this.weight + " kg heavy. He is a light panda.");
		}
	}
	public void presentHimself(){
		System.out.println("Hello! My name is " + this.name + " and I am " + this.age + " years old.");
	}
}